# No 1
![](https://gitlab.com/dwibagiasentosa/praktikum-sistem-multimedia/-/raw/main/nomor1.gif)
##### Link Youtube : https://youtu.be/EIE5y1wzJGQ


# No 2
##### Aplikasi ini merupakan aplikasi berbagai filter untuk sebuah foto berbasis CLI

### Cara Kerja

```mermaid
flowchart TD
    A[Start] --> B(Input Image file path)
    B --> C{Choose the Filter}
    C -->|Sepia Filter| D[Create 2 copy of original Image ] -->d[Convert each Image to grayscale and rgb using cvtColor Method] -->1[Apply the sepia filter by multiplying the grayscale value with a certain value] -->2[Saved the result to the rgb image] -->3[Return the image as sepia_filter] -->I
    C -->|Grayscale Filter| E[Convert the image to grayscale using cvtColor Method] -->4[Return the image] -->I
    C -->|Gaussian Blur Filter| F[Manipulate the kernel size using GaussianBlur method] -->f[Return the image] -->I
    C -->|Sobel Filter| G[Convert the image to grayscale using cvtColor method] -->g[Create 2 variable for x direction and y direction] -->5[implement the sobel method to each variable] -->6[manipulate the kernel size in each variable] -->7[Combine those two variable using addWeighted method] -->8[Return the image as sobel_filter] -->I
    C -->|Inverted Filter| H[Add the image to bitwise_not method] -->h[The method perform a bitwise not operation on each pixel of the image] -->9[Return the image as inverted_image] -->I
    I[Show the filtered image] -->J
    J{Save the image?}
    J -->|yes| j[Write the file name] -->10[Create variable to setups the compression parameters for JPG encoding that have 2 value] -->11[use cv2.IMWRITE_JPEG_QUALITY for the first value] -->12[Add some value from 0 - 100 as quality for the second value in the variable] -->13[Save the file using imwrite method] -->14[print the original image size] -->15[print the compressed image size] -->16[print the filtered image save file location]
    J -->|no| K
    K{Want to try another filter}
    K --> |yes| k[Back to main menu]
    K --> |no| l[Exit the Application]
```

# No 3
##### Untuk aspek Kecerdasan buatan pada Aplikasi ini yaitu dapat memperbaiki atau memodifikasi gambar yang diberikan menggunakan beberapa teknik pengolahan citra seperti filter Gaussian, filter Sobel, filter invert, serta filter sepia

# No 4
![](https://gitlab.com/dwibagiasentosa/praktikum-sistem-multimedia/-/raw/main/nomor4.gif)
##### Link Youtube : https://youtu.be/oPwvgUX5GH8

# No 5
##### Aplikasi ini merupakan aplikasi penambah effect reverb terhadap sebuah lagu

### Cara Kerja
```mermaid
flowchart TD
    A[Start] --> B(Input Audio file path)
    B --> C{Choose the Effect}
    C -->|Slowed Reverb| D[Define delay time and decay factor] -->d[Create a NumPy array to store the echoed audio data] -->1[Create a for loop] -->2[Iterates delay time over each sample in the input signal] -->3[the output signal is calculated by adding the current input sample to the delayed input sample with a decay factor applied] -->21[Normalized the echoed audio data by dividing it by the maximum absolute value of the signal, and scaled to a maximum amplitude of 0.5] -->22[Save the echoed audio data in temporary file called temp_reverb.wav] -->|Speed Up Reverb|23[Read the temp_reverb.wav and get the audio data and the sample rate] -->24[Multiply the sample rate by 1.2 so it increase the speed] -->25[Use the pitch_shift method] -->26[Add the n_steps and the increased sample rate] -->27[Save the shifted audio data in temporary file called temp_reverb_pitchshift.wav] -->28[Read the temp_reverb_pitchshift.wav] -->29[Use the speedup method and set the playback_speed to 1.1] -->30[Save the file as speedupreverb.wav] -->I
    C -->|Speed Up Reverb| D
    22 -->|Slowed Reverb| 31[Read the temp_reverb.wav and get the audio data and the sample rate] -->32[Multiply the sample rate by 0.85 so it decrease the speed] -->33[Use the pitch_shift method] -->34[Add the n_steps and the increased sample rate] -->35[Save the shifted audio data in temporary file called temp_reverb_pitchshift.wav] -->36[Read the temp_reverb_pitchshift.wav] -->37[Use the speedup method and set the playback_speed to 0.95] -->38[Save the file as slowedreverb.wav] -->I
    I[Print the save file location]
```

# No 6
##### Untuk aspek Kecerdasan buatan pada Aplikasi ini yaitu dapat memodifikasi Audio yang diberikan menggunakan beberapa algoritma sederhana yang disimpan dalam beberapa fungsi yaitu reverb(), pitchshift(), slowed(), dan spedup() 
